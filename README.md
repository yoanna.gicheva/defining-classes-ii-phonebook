<img src="https://i.imgur.com/yqIN5FX.png" width="300px" />

# Phonebook
Finish the implementation of the **Phonebook** application.

The application should provide the following functionality:
- List all contacts (already done)
- Add a new contact
- Remove a contact
- Update phonenumber for contact
- Search for contacts
- List contacts sorted (advanced)

## Step 1

Look at the provided skeleton and get accustomed with the code. The **listcontacts** and **addcontact** functionality is already implemented.<br>
However, we will modify the **addcontact** method - if a contact with a provided name already exists, we will not be adding it.
The `Phonebook` class is the one responsible for adding new contacts, so the `AddContact` method is the ideal place to do all necessary validations.

```cs
// in Phonebook.cs
public static void AddContact(Contact newContact)
{
    foreach (var contact in contacts)
    {
        if (contact.Name == newContact.Name)
        {
            throw new ArgumentException("Contact already exists.");
        }
    }

    contacts.Add(newContact);
}
```

Now we can no longer have duplicate contacts.
Where is the `ArgumentException` handled?

## Step 2 - Remove contact
Implement functionality to remove a contact
`removecontact pesho` - will either return `Contact removed` or `Contact pesho does not exist!`

```cs
case "removecontact":
  if (/* validate input parameters */)
  {
      throw new ArgumentException("Please provide a contact name");
  }
  return Phonebook.RemoveContact(args[1]);
```

```cs
// in Phonebook.cs
public static string RemoveContact(string name)
{
    Contact contactToRemove = null;
    foreach (var contact in contacts)
    {
        if (contact.Name == name)
        {
            contactToRemove = contact;
        }
    }

    if (contactToRemove == null)
    {
        return $"Contact {name} does not exist!";
    }

    contacts.Remove(contactToRemove);
    return $"Contact removed";
}
```

## Step 3 - UpdateContact
Implement functionality to update a contact's phonenumber
`updatecontact pesho 0888-888-888` - will either return `Contact updated` or `Contact pesho does not exist!`

- create the necessary `case` in the `switch` statement and **validate** the parameters
- create the `UpdateContact` method in the `Phonebook`
    - if the contact exists, update its `Phonenumber`
    - if there is no contact with the provided name, return the appropriate message

## Step 4 - Search for contacts
Implement functionality to search for contacts
`search p` - will return all contacts that start with `p` or `No contacts found`
```
search p
Id: 1 | pesho: [0888-888-888]
Id: 4 | penka: [0888-888-889]
```

```
search pen
Id: 4 | penka: [0888-888-889]
```
- create the necessary `case` in the `switch` statement and **validate** the parameters
- create the `Search(string startString)` method in the `Phonebook`

## Step 5 - Modify contact class (Advanced)
- Modify the `Contact` class to have `CreatedOn` property (type DateTime). There is no need to pass an argument to the constructor.
- Remove the `Id` property of the contact

```cs
// in Contact constructor:
this.Name = name;
this.PhoneNumber = phoneNumber;
this.CreatedOn = DateTime.Now; // now the instance is responsible for setting the time of its creation
```
- Modify the `GetContactInfo()` method to also include the datetime:
`pesho: [1029378] - Created on 3:35:48 PM`
- Did you have to change code outside the `Contact` class? 

## Step 6 - Modify list contacts (Advanced)
- `listcontacts` - with no parameters - list the contacts as they were added
- `listcontacts name` - list contacts sorted alphabetically by name
- `listcontacts time` - list contacts sorted by CreateOn in descending order (newer to the top)
- Ideally, most of the new logic **should** be in `ListContacts` method of the `Phonebook`. You can add an optional parameter to it.

Example:
```cs
listcontacts
pesho: [12048919] - Created on 3:45:39 PM
gosho: [87286] - Created on 3:45:50 PM
stamat: [19824901] - Created on 3:45:58 PM
apostol: [110579847] - Created on 3:46:05 PM

listcontacts name
apostol: [110579847] - Created on 3:46:05 PM
gosho: [87286] - Created on 3:45:50 PM
pesho: [12048919] - Created on 3:45:39 PM
stamat: [19824901] - Created on 3:45:58 PM

listcontacts time
apostol: [110579847] - Created on 3:46:05 PM
stamat: [19824901] - Created on 3:45:58 PM
gosho: [87286] - Created on 3:45:50 PM
pesho: [12048919] - Created on 3:45:39 PM
```

## Step 7 - Refactor CommandProcessor (Very Advanced)
Look at the switch statement in the `CommandProcessor` - it's not very pretty anymore. Let's refactor it.
- Create a folder `Commands`
- Create a class named `AddContactCommand` - it should have an `Execute` method
- Move all logic and validation to the Execute method

```cs
// in AddContactCommand.cs
public string Execute(string[] args)
{
    if (args.Length != 3)
    {
        throw new ArgumentException("Please provide at least 3 arguments. The first argument should be name, the second - phonenumber");
    }

    var contact = new Contact(args[1], args[2]);
    Phonebook.AddContact(contact);
    return $"Created contact " + contact.GetContactInfo();
}
```

`CommandProcessor` after refactoring
```cs
case "addcontact":
    var command = new AddContactCommand();
    return command.Execute(args);
case "listcontacts":
/* code */
```

- Do the same for all other functionalities - wrap them in the corresponding **Commands** with **Execute** method - e.g. `UpdateContactCommand`, `ListContactsCommand`, etc.. 

## Step 8 (Not So Very, Very, VERY Advanced - You have been warned!)
If you have done the previous task, you may have noticed that all commands are very similar - they are all `Commands` with `Execute(string[] args)` method.
If you feel brave, you can read about the wonderful OOP principles of **Polymorphism** and **Abstraction** and refactor the code so there is **no need** for a switch statement in the `CommandProcessor`.
