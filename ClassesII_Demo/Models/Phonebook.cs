﻿using ClassesII_Demo.Infrastructure;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ClassesII_Demo.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;

namespace ClassesII_Demo.Models
{
    public static class Phonebook
    {
        private static string path = @"C:\Users\yoann\OneDrive\Работен плот\GitLab - Telerik\Defining Classes II (Phonebook)\defining-classes-ii-phonebook\ClassesII_Demo\contact.json";

        private static void Serialize(List<Contact> contacts)
        {
           
            string JSONresult = JsonConvert.SerializeObject(contacts);

            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (TextWriter tw = new StreamWriter(path, true))
            {
                tw.WriteLine(JSONresult.ToString());
                tw.Close();
            }
        }
        private  static List<Contact> Deserialize()
        {
                using (TextReader file = new StreamReader(path, true))
                {
                    Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                    return (List<Contact>)serializer.Deserialize(file, typeof(List<Contact>));
                    file.Close();
                }

        }

        public static void AddContact(Contact newContact)
        {
            List<Contact> currentState = new List<Contact>();
            urrentState = Deserialize();

            foreach (var contact in currentState)
            {
                if (contact.Name == newContact.Name)
                {
                    throw new ArgumentException("Contact already exists.");
                }
            }

            currentState.Add(newContact);

            Serialize(currentState);
        }
        public static string RemoveContact(string name)
        {
            List<Contact> currentState = Deserialize();
            Contact contactToRemove = null;
            foreach (var contact in currentState)
            {
                if (contact.Name == name)
                {
                    contactToRemove = contact;
                }
            }

            if (contactToRemove == null)
            {
                return $"Contact {name} does not exist!";
            }

            currentState.Remove(contactToRemove);
            Serialize(currentState);

            return $"Contact removed";

        }

        public static string UpdateContact(string name, string phone)
        {
            List<Contact> currentState = Deserialize();


            if (currentState.Exists(x => x.Name == name))
            {
                currentState.Find(x => x.Name == name).PhoneNumber = phone;
                Serialize(currentState);

                return $"Contact updated";
            }
            else
            {

                return $"Contact {name} does not exist";
            }

        }
        public static string Search(string startString)
        {
            List<Contact> currentState = Deserialize();

            var builder = new StringBuilder();
            foreach (var contact in currentState)
            {
                if(contact.Name.Contains(startString))
                {
                    builder.AppendLine(contact.GetContactInfo());
                }
            }
            if(builder.Length == 0)
            {
                return $"No contacts found";
            }
            else
            {
                Serialize(currentState);

                return builder.ToString();
            }

        }
        public static string ListContacts()
        {
            List<Contact> currentState = Deserialize();

            var builder = new StringBuilder();
            foreach (var contact in currentState)
            {
                builder.AppendLine(contact.GetContactInfo());
            }

            return builder.ToString();


        }
        public static string ListContacts(string parameter)
        {
            List<Contact> currentState = Deserialize();

            var builder = new StringBuilder();
            if (parameter == "name")
            {
                List<Contact> ordered = new List<Contact>(currentState.OrderBy(x => x.Name).ToList());
                foreach (var contact in ordered)
                {
                    builder.AppendLine(contact.GetContactInfo());
                }
            }
            else if(parameter == "time")
            {
                List<Contact> ordered = new List<Contact>(currentState.OrderBy(x => x.CreatedOn).ToList());
                foreach (var contact in ordered)
                {
                    builder.AppendLine(contact.GetContactInfo());
                }
            }

            return builder.ToString();
        }


    }
}
