﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesII_Demo.Models
{
    public class Contact
    {
        public Contact()
        {
        }

        public Contact(string name, string phoneNumber)
        {
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.CreatedOn = DateTime.Now;
        }

        public DateTime CreatedOn { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string GetContactInfo()
        {
            return $"{this.Name}: [{this.PhoneNumber}] - Created on {this.CreatedOn}";
        }
    }
}
