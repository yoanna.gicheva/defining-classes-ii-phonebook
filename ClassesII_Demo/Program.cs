﻿using ClassesII_Demo.Infrastructure;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ClassesII_Demo.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace ClassesII_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
           
            string path = @"C:\Users\yoann\OneDrive\Работен плот\GitLab - Telerik\Defining Classes II (Phonebook)\defining-classes-ii-phonebook\ClassesII_Demo\contact.json";
            
            // CREATION OF THE JSON FILE
            List<Contact> contacts = new List<Contact>();
            string JSONresult = JsonConvert.SerializeObject(contacts);

            using (var tw = new StreamWriter(path, true))
            {
                tw.WriteLine(JSONresult.ToString());
                tw.Close();
            }

            Engine.Run();
        }
    }
}
