﻿using ClassesII_Demo.Models;
using ClassesII_Demo.Commands;
using System;

namespace ClassesII_Demo.Infrastructure
{
    public static class CommandProcessor
    {
        public static string ProcessCommand(string input)
        {
            var args = input.Split();

            switch (args[0])
            {
                case "addcontact":
                    var command = new AddContactCommand();
                    return command.Execute(args);
                case "listcontacts":
                    var commandList = new ListContactsCommand();
                    return commandList.Execute(args);
                case "removecontact":
                    var commandRemove = new RemoveContactCommand();
                    return commandRemove.Execute(args);
                case "updatecontact":
                    var commandUpdate = new UpdateContactCommand();
                    return commandUpdate.Execute(args);
                case "search":
                    var commandSearch = new SearchCommand();
                    return commandSearch.Execute(args);
                default:
                    return "Invalid command";
            }
        }
    }
}
