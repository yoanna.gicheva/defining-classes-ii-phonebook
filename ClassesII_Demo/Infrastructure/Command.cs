﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;
using ClassesII_Demo.Commands;


namespace ClassesII_Demo.Commands
{
     abstract class Command
    {
          public abstract string Execute(string[] args);
    }
}
