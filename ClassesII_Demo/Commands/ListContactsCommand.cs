﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;



namespace ClassesII_Demo.Commands
{
    class ListContactsCommand : Command
    {
          public override string Execute(string[] args)
        {
            if (args.Length != 2)
            {
                if (args[0] == "name" || args[0] == "time")
                {
                    return Phonebook.ListContacts(args[0]);
                }
                else
                {
                    throw new ArgumentException("Please provide a contact name or a time");
                }
            }
            return Phonebook.ListContacts();
        }
    }
}
