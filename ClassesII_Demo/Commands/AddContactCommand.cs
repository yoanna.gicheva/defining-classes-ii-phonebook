﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    class AddContactCommand : Command
    {
        public override string Execute(string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide at least 3 arguments. The first argument should be name, the second - phonenumber");
            }

            var contact = new Contact(args[1], args[2]);
            Phonebook.AddContact(contact);
            return $"Created contact " + contact.GetContactInfo();
        }

    }
}
