﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    class RemoveContactCommand : Command
    {
         public override string Execute(string[] args)
        {
            if (args.Length != 2)
            {
                throw new ArgumentException("Please provide a contact name");
            }
            return Phonebook.RemoveContact(args[1]);
        }
    }
}
