﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    class SearchCommand : Command
    {
         public override string Execute(string[] args)
        {
            if (args.Length != 2)
            {
                throw new ArgumentException("Please provide a contact name to be searched");
            }
            return Phonebook.Search(args[1]);
        }

    }
}
