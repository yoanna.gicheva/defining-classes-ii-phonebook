﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    class UpdateContactCommand : Command
    {
         public override string Execute(string[] args)
         {
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide a contact name and contact number");
            }
            return Phonebook.UpdateContact(args[1], args[2]);
         }
    }
}
